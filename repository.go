package main

import (
	"context"
	"encoding/json"
	"os/exec"

	pb "gitlab.com/lazybasterds/alpaca/weather-service/proto/weather"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetWeather(ctx context.Context, locale string) (*pb.Weather, error)
}

//WeatherRepository concrete implementation of the Repository interface.
type WeatherRepository struct {
}

// GetWeather gets the weather for the location specified.
// Request time based on the city name and country code (e.g. Berlin,DE, Singapore,SG, Manila,PH, etc.)
// from https://openweathermap.org/ .It will return a Weather object.
func (repo *WeatherRepository) GetWeather(ctx context.Context, locale string) (*pb.Weather, error) {

	if locale == "" {
		locale = "Singapore,SG"
	}

	apiCode := "d7615e0d00ad461dd38071bee7b61fd8"
	requestURL := "https://api.openweathermap.org/data/2.5/weather?q=" + locale + "&APPID=" + apiCode

	curl := exec.Command("curl", requestURL)
	out, err := curl.Output()

	if err != nil {
		return nil, err
	}

	var weather Weather
	err = json.Unmarshal(out, &weather)

	if err != nil {
		return nil, err
	}

	//Compute for temperature in C and F
	tempInK := weather.Main.TempInK
	weather.Main.TempInC = tempInK - 273.15
	weather.Main.TempInF = (tempInK-273.15)*9/5 + 32

	result := pb.Weather{
		Id:   weather.ID,
		Name: weather.Name,
		Code: weather.Code,
		Coord: &pb.CoordInfo{
			Latitude:  weather.Coord.Latitude,
			Longitude: weather.Coord.Longitude,
		},
		Base: weather.Base,
		Main: &pb.MainInfo{
			Tempink:  weather.Main.TempInK,
			Tempinc:  weather.Main.TempInC,
			Tempinf:  weather.Main.TempInF,
			Pressure: weather.Main.Pressure,
			Humidity: weather.Main.Humidity,
			Tempmin:  weather.Main.TempMin,
			Tempmax:  weather.Main.TempMax,
		},
		Visibility: weather.Visibility,
		Wind: &pb.WindInfo{
			Speed:  weather.Wind.Speed,
			Degree: weather.Wind.Degree,
		},
		Clouds: &pb.CloudsInfo{
			All: weather.Clouds.All,
		},
		Datetime: weather.DateTime,
		System: &pb.SystemInfo{
			Id:      weather.System.ID,
			Type:    weather.System.Type,
			Message: weather.System.Message,
			Country: weather.System.Country,
			Sunrise: weather.System.Sunrise,
			Sunset:  weather.System.Sunset,
		},
	}

	tmpW := make([]*pb.WeatherInfo, 0, len(weather.Weather))
	// For Weather Info
	for _, w := range weather.Weather {
		pbw := &pb.WeatherInfo{
			Id:          w.ID,
			Main:        w.Main,
			Description: w.Description,
			Icon:        w.Icon,
		}

		tmpW = append(tmpW, pbw)
	}

	result.Weather = tmpW

	return &result, nil
}
