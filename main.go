package main

import (
	"log"
	"os"

	micro "github.com/micro/go-micro"
	_ "github.com/micro/go-plugins/registry/kubernetes"
	k8s "github.com/micro/kubernetes/go/micro"
	pb "gitlab.com/lazybasterds/alpaca/weather-service/proto/weather"
)

const (
	defaultHost = "localhost:27017"
)

var (
	srv micro.Service
)

func main() {
	host := os.Getenv("DB_HOST")

	if host == "" {
		host = defaultHost
	}

	srv = k8s.NewService(
		micro.Name("weather-service"),
		micro.Version("latest"),
	)

	// Will parse the command line args
	srv.Init()

	// Register handler
	pb.RegisterWeatherServiceHandler(srv.Server(), &service{})

	// Run the server
	if err := srv.Run(); err != nil {
		log.Printf("ERROR: Failed to run server for weather-service - %v", err)
	}
}
