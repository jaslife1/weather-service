package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/weather-service/proto/weather"
)

type service struct {
}

func (s *service) GetRepo() Repository {
	return &WeatherRepository{}
}

func (s *service) GetWeather(ctx context.Context, req *pb.Locale, res *pb.WeatherResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	locale := req.Locale
	weather, err := repo.GetWeather(ctx, locale)
	if err != nil {
		log.Printf("Error: Failed to get Weather for locale %s : %+v", locale, err)
		return err
	}
	log.Printf("Method: GetWeather, RequestID: %s, Return: %+v", requestID, weather)
	res.Weather = weather
	return nil
}
